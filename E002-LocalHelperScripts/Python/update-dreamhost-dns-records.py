#!/usr/bin/env python3

# Update Dreamhost DNS Records
# Author:   David "TrueKam" Collins (dcollins@6fpsnetwork.com)
# Version:  1.0.0γ
# Date:     March 27th, 2023
#
# This script will update DNS records on Dreamhost. Its intent is for use as a scheduled task to create
# a do-it-yourself dynamic DNS service using your external IP address.
#
# Requirements:
# - Python 3
# - urllib
#
# Instructions:
# - Update 'apiKey' with your API key from https://panel.dreamhost.com/?tree=home.api
# - Update 'recordKey' with the subdomain to add. Dreamhost requires the entire FQDN.
# - (Optional) Update 'typeKey' with the DNS record type. See https://en.wikipedia.org/wiki/List_of_DNS_record_types
# - (Optional) Update 'valueKey' with the value of the record. If left blank, the external IP address of the system
#   running this script will be used.
# - (Optional) Update 'commentKey' to add a comment to the DNS record.
# - Run the script using the Python interpreter.
#
# Considered Additions:
# - Parameters or config file instead of editing made to this script for values.
# - API URL customization for use on hosts other than Dreamhost.
# - Verbose/Logging output.
# - Error handling.
# - Move documentation to wiki/web.
# - Add a version history log. (There isn't a history at the moment, so no log needed!)

from urllib.parse import quote
from urllib.request import urlopen

# User Configuration Section Start
apiKey = "1234567890"
recordKey = "subdomain.domain.com"
typeKey = "A"
valueKey = ""
commentKey = ""
# User Configuration Section End

ipAddressQueryURL = "https://ipapi.co/ip/" # Alternatively: https://api.ipify.org if ipapi.co stops functioning.
dreamhostApiUrl = "https://api.dreamhost.com"

# If the valueKey is blank, assume that it's being used to hold the external IP address and get it.
if not valueKey:
    valueKey = urlopen(ipAddressQueryURL).read()

# Get a list of records that match the recordKey provided. There's no way to update a record nor is there a way to remove
# the record without knowing its value already. This will help with that removal process.
apiListRecordsUrl = dreamhostApiUrl + "/?key=" + apiKey + "&cmd=dns-list_records"
recordsList = []
recordsRaw = urlopen(apiListRecordsUrl)
recordsLinesArray = (recordsRaw.read()).decode("utf-8)").split('\n')
for currentRecord in enumerate(recordsLinesArray):
    if recordKey in currentRecord[1]:
        recordsList.append(currentRecord[1].split('\t'))

# Remove any entries found for the recordKey provided.
for currentRecord in enumerate(recordsList):
    apiRemoveRecordUrl = dreamhostApiUrl + "/?key=" + apiKey + "&cmd=dns-remove_record&record=" + recordKey + "&type=" + typeKey + "&value=" + currentRecord[1][4]
    urlopen(apiRemoveRecordUrl)


# Add the new request with the current external IP address.
# Construct the URL to issue the request.
apiAddRecordUrl = dreamhostApiUrl + "/?key=" + apiKey + "&cmd=dns-add_record&record=" + recordKey + "&type=" + typeKey + "&value=" + valueKey.decode("utf-8")

# If there was a comment provided above, add it to the URL as well.
if commentKey:
    commentKey = "&comment=" + quote(commentKey)
    apiAddRecordUrl += commentKey

# Make a call to the API URL to add the DNS record.
urlopen(apiAddRecordUrl)