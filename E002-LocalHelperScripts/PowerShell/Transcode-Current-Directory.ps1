<#
    Transcode-Current-Directory.ps1
    David "TrueKam" Collins
    
    This script converts all the video files in the current directory to x265 and puts the output into the ./Output
    directory. If the ./Output directory does not exist, it is created.

    Prerequisites:
        - ffmpeg installed and available on path.
#>

if (-not (Test-Path C:\Windows)) {
    mkdir Output
}

Get-ChildItem -File | ForEach-Object {Invoke-Expression "ffmpeg -i '$($_.Name)' -map 0 -c:v libx265 -c:a copy -c:s copy '.\Output\$($_.Name)'"}
<#
                                                         Invoke the ffmpeg executable taking the name of the file from 'foreach'.
                                                                                 Use the first stream found.
                                                                                        Transcode the video using x265.
                                                                                                     Copy the audio stream(s) exactly.
                                                                                                               Copy the subtitle stream(s) exactly.
#>