package e001;

import java.util.Arrays;

/**
 * @author David Collins (dcollins@6fpsnetwork.com)
 * 
 * This is a sample implementation of the rSort algorithm created by myself. The
 * code is not very extensible and is meant to serve as an example only.
 *
 * Please note that this sorting algorithm should never be used in a production
 * environment. Ever.
 **/
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Local Data
                                    // Our original values. For this basic test,
                                    // I have chosen to use some simple integer
                                    // values. If you're feeling a bit more
                                    // brave, you can uncomment the second
                                    // set instead.
        int[] testIntegerValues = {9, 8, 16, 4, 35, 12, 16};
        /*
        int[] testIntegerValues = { 1456, 4645, 7988, 3240, 6837, 3980, 6178, 
                                    2714, 9565, 2507, 4162, 6386, 4496, 8256, 
                                    7020, 3522, 9686, 1841, 4867, 8252, 4922, 
                                    4848, 2283, 8750, 5328, 3638, 2811, 9538, 
                                    1725,  770, 6641, 9015, 6679, 7961, 3845, 
                                     695, 1391, 4705, 6734,   87, 2673, 6014, 
                                    9718, 4047, 9256, 5301, 9431, 5723, 4319, 
                                    8478, 8304, 3327, 2998, 5343, 1275, 1550, 
                                    8397, 3237, 5344,  903, 6443,  526, 1792, 
                                    8313, 1189, 6542,  402, 4321,  874, 3735, 
                                    2879, 5129, 4071, 4718, 7833, 7868, 7824, 
                                    3362, 5475, 9223, 6215, 5802, 1245,   73, 
                                    3625, 1252, 6111, 4159,  945, 5551, 7961, 
                                     465, 7906, 3895, 7695, 3979, 1044, 7483, 
                                    5916, 2569, 4455, 1051, 7110, 3056, 7524, 
                                    5216, 9760, 2440, 5614, 1349, 6975,   42, 
                                    2681,  231, 6005, 4630, 5464, 1556, 2199, 
                                    4177, 7612, 3762, 2502,  202, 6201, 2335, 
                                    7663, 3723, 1900, 2140, 8915, 2984, 9641, 
                                    9381, 2289,  966, 4755, 4421,  518, 1148, 
                                    4520, 4061, 6255, 8580, 7765, 2701,   33, 
                                    4827, 4296, 3657, 7317, 2902, 5622, 1234, 
                                    8801, 3217, 8564, 5365, 2960, 2591,  549, 
                                    6208, 8738, 8181,  881, 6216, 5689, 6086, 
                                    6639,  435, 9596,  273, 8432, 9795, 7176, 
                                    7316, 6784, 2892, 9951, 7368, 3451, 5902, 
                                    9903, 8632, 7965, 9804,  982, 2952, 5560, 
                                    7680,  401, 8330, 5565, 1930, 1471, 8640, 
                                    2221, 8838,  497, 5169, 5109, 2822, 9212, 
                                    4715, 8135,  414, 9659, 2934,  341, 1387, 
                                      29, 2284, 8703, 3010, 7036, 8029, 2795, 
                                    2291,   63, 2980, 9555,  980, 2265, 9040, 
                                     503, 3058, 9985, 3721, 4143, 5939,  630, 
                                    7849, 2811, 9074, 1471, 6291, 4638, 4122, 
                                    6275, 9258, 5731, 7246, 6080, 3915, 6044, 
                                    7779, 7951, 5589,  638, 2197, 3779, 6520, 
                                    2534,  788, 4809, 3082, 4262, 7430, 6369, 
                                    9493, 8115,  766, 3490, 3020, 5312, 7115, 
                                     190, 6473, 1360, 7721, 8701, 9546, 3762, 
                                    2728, 9501,  363, 9051, 6875, 1993,  383, 
                                    5327, 5718, 8062, 8174, 9143,  607, 4509, 
                                    8350, 8997, 2784, 9936, 7746, 2157,   85, 
                                    7672, 1515,  645, 3461, 7769, 5174, 3964, 
                                    6246,  555, 7049, 4637, 3476, 1776,  315, 
                                    8824, 4853, 2812, 4716,  597, 7482, 6984, 
                                    3837, 1519, 9586, 3235, 1883, 9612, 1018, 
                                    2556, 1112, 6228, 9103, 2709, 7314, 3898, 
                                     682, 5926, 2317, 6725, 5360, 4510,  745, 
                                    1965, 3965, 4942, 1410, 1628, 9149, 8333, 
                                    4223, 2426, 9210, 4274, 8299, 7544, 5071, 
                                    2913, 1333, 1595, 2573, 8613, 5798, 4137, 
                                    7889,  914, 6408, 3026, 8122, 2144, 9857, 
                                    1815, 3091, 4332, 9874, 7176, 3695, 4385, 
                                    3113, 1494,  707, 8534, 9071, 8829, 7434, 
                                    6094, 1224, 7405, 8763,  527, 1065, 3962, 
                                    7700, 3536, 6232, 6343, 6018, 3887,  338, 
                                    7602, 1336, 1647, 2265, 2518,  780, 6076, 
                                    7148, 4817, 7138, 9341, 7206, 1341, 4407, 
                                    1351,  291, 7164, 1883, 6198, 4999, 2624, 
                                    3689, 2507, 6256, 6141, 2731,  401, 6816, 
                                    3349, 6055, 9030, 9176, 5331, 3251, 7836, 
                                    3076, 1028,  448, 9174, 1205, 5894, 3776, 
                                    7941, 5163, 3943, 5397, 8523, 3305, 6331, 
                                    9963, 4235, 3611, 7748, 6193, 6861,  863, 
                                    5677, 4548, 8371, 7896, 6271,  952, 6534, 
                                    6632, 3570, 3577, 2832, 2893, 6353,  553, 
                                    6596, 4735, 9357, 4633, 7237, 2130, 4332, 
                                    8108, 8660, 2382, 9188, 8531, 5153, 3108, 
                                     347,  316, 8228,  818, 1179, 4040, 4289, 
                                     538, 7987, 6793, 9710,  584,  771,   52, 
                                    9699, 8995, 8258, 3741, 5784, 2298,   27, 
                                    1467,  701, 4527, 8464, 6726, 9496,  883, 
                                    2154, 4971, 4561, 4138, 2256, 8828, 8941, 
                                    7965, 1558, 9238, 8978, 1879, 8159,  917, 
                                    6938, 4265, 6539, 9851, 2710, 5021,   41, 
                                    8906,  744, 3831, 2148, 6257, 3884, 1621, 
                                    7581, 3207, 5528, 8150, 8929, 2391, 1546, 
                                    9581, 6280, 5402, 8051, 5326, 6957, 1607, 
                                     593, 9799, 6436, 8764, 9228, 1342, 9719, 
                                    3758, 8380, 6323,  686, 3068,  233, 1322, 
                                    2129, 4246, 7120, 2206, 9932, 7377, 1106, 
                                    8127, 8647,  474, 7079, 2379, 1743,   32, 
                                    9156, 3716, 4411, 8717, 7202, 4803, 8105, 
                                    4231, 9783, 6278,  215, 5230, 6922, 4402, 
                                    6629, 4379, 7802, 4949, 9083, 8919, 2548, 
                                    7235, 6842, 4048, 1427,  476, 1013, 9965, 
                                    4091, 4773, 9245, 4404, 1812, 7995, 8148, 
                                    4439, 6991, 3879, 9481,  576, 2237, 5804, 
                                    6790, 4805,  806, 3134, 4319, 8547, 7649, 
                                    9282, 3343, 1340, 5428, 9197, 3860, 9302, 
                                    9129, 2452, 4461, 9508, 5851, 2896, 7643, 
                                    5996,  311, 4776, 9923, 8274, 3056, 8473, 
                                    7476, 8204, 1021, 5062, 4403, 2845, 8663, 
                                     974, 9577,  199, 9462, 7624, 3315, 4341, 
                                    1862, 7613, 6309, 2042, 4849, 8241,  173, 
                                    8193, 2027, 4969, 6392, 3293, 7255, 4581, 
                                    2392, 8127, 3220, 1140, 4468, 6313, 5302, 
                                    3364, 4417, 1558, 9717, 7120, 2228, 6960, 
                                    3071, 5076, 6010, 1101, 1073, 5773, 3928, 
                                    3797, 8988, 9051, 7742, 3003, 6477, 5841, 
                                    8823, 8977, 8934, 1455, 3359, 4265, 8738, 
                                    5609, 3361, 6620, 3780, 6887, 6599, 9198, 
                                    7050, 6668, 9210, 7378,  673, 3411, 8332, 
                                    3287,  816, 9028,  876,  434, 9449, 6066, 
                                    6150, 5912, 8109, 7338,  793, 2114, 3167, 
                                    9428, 6655, 3558, 3370, 3218, 2060, 5579, 
                                    9349, 6259, 7167, 1685, 9496, 1326,  228, 
                                    9409, 2296, 2044, 5850, 8752, 3582,  611, 
                                    9198,  909, 6393, 7825, 6378, 3287, 4801, 
                                    3028, 5929, 8732, 9804, 7968, 8845, 9485, 
                                    3701,  978, 3629, 7017, 1608, 2373, 2849, 
                                    4351,  889, 1635, 6645, 9402, 1416, 9593, 
                                    4165, 7903, 2301, 8567, 3253,   84, 3635, 
                                    6507, 4163, 3576, 3684, 4634, 3642, 5922, 
                                     850, 6101, 2342, 5464, 5091,  784, 6094, 
                                     290, 5627, 6592, 2059, 1413, 6178, 8106, 
                                    9440, 4181, 6984, 5618, 9782, 5276, 5026, 
                                     736, 9428, 3914, 7629, 4163, 4576, 9084, 
                                    9413,  616, 3444, 3115, 1795, 9801, 1785, 
                                    6779, 3402, 8020, 1481, 1588, 4379, 9198, 
                                    9438, 8718, 4643, 2132, 8685, 1442, 3142, 
                                    3249, 2297,  296, 1995, 8742, 4591, 7198, 
                                    3669, 5332, 1011, 9447, 9265, 2800, 1839, 
                                    3438, 4491, 5014, 2503, 8764, 8736, 2325, 
                                     824,  340, 3630, 1928,  639,  114, 5199, 
                                    4909, 9133, 2896, 6759, 4463, 7096, 9110, 
                                    7112, 1741, 7657, 9350, 7046, 5335, 5760, 
                                    5020,  260, 7234,  670, 2811, 7070, 9262, 
                                    1295, 9138, 3640, 9351, 8193, 8418, 7003, 
                                    7838, 8668, 7004, 6941,   41, 5668, 4356, 
                                    9012,  623, 5918, 1113, 8750,  263, 1419, 
                                    1610,  214, 6834, 9976, 5206, 9946, 8140, 
                                    3685, 3686, 4236, 4807, 6878, 3295, 5108, 
                                    4850, 2242, 6158, 5719, 1309, 6013, 9455, 
                                    9533, 8181, 4672, 9459,  950, 9658,  345, 
                                    2936, 6965, 6624, 2923, 4651, 6350, 1594, 
                                    5138, 9753, 8735, 5404, 7042, 5339, 8729, 
                                    5159, 3275, 3841, 9152, 5307, 9429, 9860, 
                                    1752,  882, 1192,  896, 1701, 8108, 4642, 
                                     997, 7848, 8057, 7813, 6344, 2731, 8040, 
                                    4361, 7503, 9620, 1852,  825, 9364, 8997, 
                                    7681, 3095, 5128, 8197, 3848, 1160, 5409, 
                                    5254,  794,  358, 2976, 7394, 5759, 8465 };
        */
        int[] workIntegerValues;    // This is where we'll sort our values to.
        int[] sortedIntegerValues = testIntegerValues;
        Arrays.sort( testIntegerValues );
                                    // This is the expected final sorted order
                                    // of the integers. I wanted to make this
                                    // check simple instead of writing a whole
                                    // "isSorted" method.
        
        boolean sorted = false;     // This tracks whether the data is sorted or
                                    // not.
        boolean verbose = true;    // Do you want to have more verbose output
                                    // during the program runtime?
        
        int iterations = 1;         // This will track how many iterations of
                                    // the sort method we run. This will allow
                                    // us some diagnostic output as well as
                                    // ensuring that if we go too long, we can
                                    // stop the sorting attempts.
        long maxIterations = 100_000_000;
                                    // How many sorting attempts would you like
                                    // to make? Be aware that large values may
                                    // make your computer cry itself to sleep
                                    // and may also convince you that nothing is
                                    // really being dones except a consistant
                                    // spike of CPU usage. I recommend setting
                                    // "verbose" to true so you can get a little
                                    // extra feedback at set intervals.
        
        

        // Logic
        while (!sorted && iterations < maxIterations) {
                                    // This will continue to run until we either
                                    // have a sorted list or we have reached the
                                    // predefined limit to our iterations.
            workIntegerValues = rSort(testIntegerValues);
                                    // This is the actual method call to
                                    // /attempt/ a sort.
            if (Arrays.equals(workIntegerValues, sortedIntegerValues)){
                                    // I'm using the predefined "sorted" array
                                    // just to check if the initial values are
                                    // sorted. This can be replaced in the
                                    // future with a more comprehensive
                                    // "isSorted" method to check the array
                                    // dynamically instead of requiring a sorted
                                    // list to be put in at compile time.
                if (verbose)
                    System.out.println(" Verbose Output: The data has been "
                            + "sorted.");
                sorted = true;
                                    // If we're sorted, we can exit the loop and
                                    // proceed. To be fair, we probably won't
                                    // get here, so your tests may seem to
                                    // indicate that this part of the code is
                                    // never reached.
            } else {
                if (verbose && (iterations % 1000) == 0) 
                    System.out.println(" Verbose Output: Not Sorted. "
                            + "Iterations: " + iterations);
                                    // Note the modulus operator here. Having
                                    // the line print out with each iteration is
                                    // just insane, even if the user requested
                                    // that the program provide verbose output.
                iterations++;
            }
        }
        
        // Summary Output
        if (sorted) {
            System.out.println(" The data was successfully sorted "
                    + "after " + iterations + " iterations.");
        } else {
            System.out.println(" The data was not successfully sorted.");
        }
    }
    
    private static int[] rSort(int[] incomingArray) {
        
        // Local Data
        int[] outgoingArray = new int[incomingArray.length];
                                    // This is where we'll store the outgoing
                                    // array. Note that it is specifically only
                                    // an integer array in this sample
                                    // application. You will have to alter this
                                    // if you decide to use this sorting
                                    // algorithm in your own software.
        boolean readyToContinue = false;
        int randomIndex;
                                    // These are just for housekeeping purposes.
                                    // The boolean is meant to control whether
                                    // we can progress after a particular index
                                    // (more on that in a moment) and the
                                    // integer is just to figure out which index
                                    // we're going to work with at any given
                                    // time.
        
        boolean[] doneArray = new boolean[incomingArray.length];
        for (int i = 0; i < doneArray.length; i++)
            doneArray[i] = false;
                                    // This is a special array that is meant to
                                    // only hold a booleanmvalue to determine if
                                    // the randomly chosen index has already
                                    // been copied to the outgoing array.
                                    // Without this array to keep things in
                                    // order, we would potentially run into a
                                    //situation where the outgoing array has
                                    // multiple copies of elements in the
                                    // incoming array. Needless to say, if we
                                    // aren't checking the original array versus
                                    // a new array with the same values, it can
                                    // never actually be a sorted list of the
                                    // elements in the original array, can it?
        
        for (int i = 0; i < incomingArray.length; i++) {
                                    // We need to cycle through the outgoing
                                    // array a single time
            do {
                                    // and choose an index to copy from the
                                    // incoming array.
                randomIndex = (int)(Math.random() * (incomingArray.length));
                if (doneArray[randomIndex] == false) {
                    doneArray[randomIndex] = true;
                    outgoingArray[i] = incomingArray[randomIndex];
                    readyToContinue = true;
                }
                                    // But we need to ensure that the element in
                                    // any particular index in the incoming
                                    // array hasn't already been added to the
                                    // outgoing array. As long as we hit values
                                    // that have already been used,
                                    // readyToContinue will remain false and we
                                    // won't progress. This ensures that we will
                                    // always get each value and only one time
                                    // each.
            } while (!readyToContinue);
                                    // Reset readyToContinue for the next
                                    // iteration of the loop.           
            readyToContinue = false;
        }
                                    // At this point, we are all done going
                                    // through the outgoing array and adding the
                                    // values from the incoming array, so we can
                                    // simply...        
        return outgoingArray;
    }
}
